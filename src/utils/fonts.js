
const h1Font = "Roboto-Regular";
const h2Font = "Roboto-Bold";
const h4Font = "Roboto-Regular";
const h5Font = "Roboto-Medium";
const h6Font = "Roboto-Bold";
const h7Font = "Roboto-Medium";
const mainFont = "Roboto";
const body1 = "Roboto-Regular";
const body4 = "Roboto-Regular";

export {
    h1Font,
    h2Font,
    h4Font,
    h5Font,
    h6Font,
    h7Font,
    mainFont,
    body1,
    body4,
}