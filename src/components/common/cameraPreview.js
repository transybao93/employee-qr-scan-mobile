import React, { Component } from 'react'
import { 
    Text, 
    View, 
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native'
//- import color
import {
    bgColor
} from '../../utils/color';
  
  //- margin
  import {
    left,
    right
} from '../../utils/margin';

//- import font
import {
    h6Font
} from '../../utils/fonts';

import BasicHeader from './basicHeader';
import { Button } from 'react-native-elements';


export class CameraPreview extends Component {

    constructor(props){
        super(props)
        this.state = {
            params: this.props.navigation.state.params,
            imageURI: this.props.navigation.state.params.imageURI,
        }

    }

    render() {

        const {
            currentDateTime,
            currentLocation,
            buttonText,
            type
        } = this.state.params;

        console.log('type in camera preview', type)
        return (
            <View style={styles.container}>
                <BasicHeader navigation={this.props.navigation}/>
                <View style={styles.pictureArea}>
                    <Image
                        style={{width: "100%", height: "100%", borderRadius: 6}}
                        source={{uri: this.state.imageURI}}
                    />
                </View>
                <View style={styles.locationTimeArea}>
                    <Text>{currentDateTime}</Text>
                    <Text>{currentLocation}</Text>
                </View>
                <View style={styles.buttonArea}>
                    <TouchableOpacity style={styles.buttonStyle}>
                        <Text style={styles.buttonTextStyle}>
                            {(type === 'checkIn') && 'Check in' || (type === 'checkOut') && 'Check out'}
                        </Text>
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

export default CameraPreview

const styles = StyleSheet.create({

    container: {
        flex: 1,
        paddingLeft: left,
        paddingRight: right,
        backgroundColor: bgColor,
    },
    pictureArea: {
        flex: 4,
        paddingTop: 10,
        paddingBottom: 5
    },
    locationTimeArea:{
        flex: 1,
    },
    buttonArea:{
        flex: 1,
    },
    buttonStyle: {
        width: '100%',
        height: 46,
        backgroundColor: '#00224B',
        color: 'white',
        borderRadius:6,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 22
    },
    buttonTextStyle: {
        color: 'white',
        fontFamily: h6Font,
        fontSize: 16,
    }

});