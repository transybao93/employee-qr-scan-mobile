import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    Button
} from 'react-native'
import PropTypes from 'prop-types';

//- import color
import {
    bgColor
} from '../../utils/color';
  
//- margin
import {
    left,
    right
} from '../../utils/margin';

//- fonts
import {
    h4Font
} from '../../utils/fonts';

export class BasicHeader extends Component {


    render() {
        const {
            headerTitle,
            navigation,
            haveRightButton,
            rightButtonText,
            haveBackButton
        } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.left}>
                    {(haveBackButton) && 
                        <TouchableOpacity style={{width: 50}} onPress={() => navigation.pop()}>
                            <Image
                                source={require('../../assets/images/icons/back2.png')}
                            />
                        </TouchableOpacity>
                    }
                </View>
                <View style={styles.center}>
                    <Text style={styles.headerTitle}>{headerTitle}</Text>
                </View>
                <View style={styles.right}>
                    {/* right icon */}
                    {haveRightButton &&
                    <Button
                        style={{color: 'white'}}
                        title={rightButtonText}
                        onPress={this.props.rightButtonOnPress}
                    />
                    }
                </View>
            </View>
        )
    }
}

export default BasicHeader

const styles = StyleSheet.create({

    container: {
        flex: 0.5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'white'
    },
    left: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    right: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerTitle: {
        fontFamily: h4Font,
        fontSize: 18
    },

})

//- prop types
BasicHeader.propTypes = {
    headerTitle: PropTypes.string,
    haveRightButton: PropTypes.bool,
    rightButtonText: PropTypes.string,
    haveBackButton: PropTypes.bool
}

//- default
BasicHeader.defaultProps = {
    headerTitle: "Home",
    haveRightButton: false,
    haveBackButton: false
}