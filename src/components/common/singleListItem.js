import React, { Component } from 'react';
import { 
    View, 
    Text,
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native';

class SingleListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        const {
            item
        } = this.props;
        return (
            <TouchableOpacity style={styles.container}>
                <View style={styles.left}>
                    <Image style={styles.image} source={require('../../assets/images/avatar.jpg')}/>    
                </View>
                <View style={styles.right}>
                    <Text> Ngày log: {item.lDate} </Text>
                    <Text> Thời gian log: {item.lTime} </Text>    
                </View>
            </TouchableOpacity>
        );
    }
}

export default SingleListItem;
const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 20,
        backgroundColor: 'white',
        paddingTop: 10,
        paddingBottom: 10,
    },
    left: {
        flex: 1,
        alignItems: 'center',
    },
    right: {
        flex: 3
    },
    image:{
        width: 60,
        height: 60,
        borderRadius: 60/2,
        overflow: 'hidden',
        // alignSelf: 'center',
        // marginBottom: 16,
        // resizeMode: 'contain',
        // marginTop: 15,
    },


});