import React, { Component } from 'react'
import { 
    Text, 
    View, 
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types';
import moment from 'moment';

//- margin
import {
    top
} from '../../utils/margin';

//- font
import {
    h2Font,
    h4Font,
    h6Font
} from '../../utils/fonts';

//- color
import {
    title,
    subHeader1,
    subHeader2,
    hrLine,
} from '../../utils/color';

export class Header extends Component {

    constructor(props)
    {
        super(props)
    }

    _onAvatarPress = () => {
        this.props.navigation.navigate('Profile');
    }

    _onRightHeaderTextPress = () => {
        this.props.navigation.navigate(this.props.rightHeaderPage);
    }

    render() {
        const { 
            title, 
            titleFontSize, 
            welcomeText, 
            dateTimeText, 
            userName, 
            isDisplayWelcomeText, 
            isDisplayDateTimeText,
            isMarginTop,
            marginTop,
            haveBottomLine,
            isShowAvatar,
            isMarginLeft,
            left,
            isMarginBottom,
            bottom,
            isShowRightHeaderTitle,
            rightHeaderTitleText,
        } = this.props;
        console.log('title', title)

        return (
            <View style={[styles.container, {marginLeft: (isMarginLeft)? left:0, marginBottom: (isMarginBottom) ? bottom: 0}]}>
                <View style={{flex: 1, flexDirection: 'row'}}>
                    <Text style={[styles.title, {flex: 1, 'fontSize': titleFontSize, 'marginTop': isMarginTop ? marginTop : top}]}>{title}</Text>
                    <View style={{
                        flex: 1,
                        justifyContent: 'flex-end',
                        alignItems: 'flex-end',
                        alignContent: 'flex-end',
                        display: isShowRightHeaderTitle ? 'flex' : 'none'
                    }}>

                        <TouchableOpacity style={{
                            width: 50, 
                            height: 20,
                        }}
                        onPress={this._onRightHeaderTextPress}
                        >
                            <Text>{rightHeaderTitleText}</Text>
                        </TouchableOpacity>

                    </View>
                </View>
                {/* Sub header */}
                <View style={styles.subHeader}>

                    {
                        (isDisplayWelcomeText || isDisplayDateTimeText) && 
                        <View style={styles.subHeaderLeft}>
                        { (isDisplayWelcomeText === true) && <Text style={styles.welcomeText}>{welcomeText + userName}</Text>}
                        { (isDisplayDateTimeText === true) && <Text style={styles.dateTimeText}>{dateTimeText}</Text>}
                        </View>
                    }

                    {isShowAvatar && 
                        <View style={styles.subHeaderRight}>

                            <TouchableOpacity onPress={this._onAvatarPress}>
                                <Image
                                    style={styles.image}
                                    source={require('../../assets/images/avatar.jpg')}
                                    resizeMode="contain"
                                />
                            </TouchableOpacity>
                        </View>
                    }
                    
                </View>
                <View style={haveBottomLine && styles.hrLine}></View>
            </View>
        )
    }
}

export default Header

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: 'yellow',
    },

    // sub header
    subHeader: {
        flex: 1,
        flexDirection: 'row',
        height: 'auto',
        // paddingBottom: 14
        // backgroundColor: 'black',
    },
    subHeaderLeft: {
        flex: 2,
        // backgroundColor: 'black'
    },
    subHeaderRight: {
        flex: 1,
        // backgroundColor: 'yellow',
    },
    image: {
        // flex:1, 
        justifyContent: 'center',
        alignSelf: 'flex-end',
        height: 44, 
        width: 44,
        borderRadius: 22,
    },

    title: {
        fontSize: 36,
        fontFamily: h2Font,
        color: title
    },
    //- sub header 1
    welcomeText: {
        fontSize: 18,
        fontFamily: h4Font,
        color: subHeader1,
        marginTop: 5,
    },
    //- sub header 2
    dateTimeText: {
        fontSize: 14,
        fontFamily: h6Font,
        color: subHeader2,
        marginTop: 5,
    },
    hrLine: {
        height: 5,
        borderBottomWidth: 0.5,
        borderBottomColor: hrLine,
    },
    display: {
        display: 'flex',
    },
    notDisplay: {
        display: 'none',
    },
});

//- prop types
Header.propTypes = {
    isShowSubTitle: PropTypes.bool,
    title: PropTypes.string,
    welcomeText: PropTypes.string,
    dateTimeText: PropTypes.string,
    userName: PropTypes.string,
    isDisplayDateTimeText: PropTypes.bool,
    isDisplayWelcomeText: PropTypes.bool,
    titleFontSize: PropTypes.number,
    isMarginTop: PropTypes.bool,
    haveBottomLine: PropTypes.bool,
    isShowAvatar: PropTypes.bool,
    isMarginLeft: PropTypes.bool,
    left: PropTypes.number,
    isShowRightHeaderTitle: PropTypes.bool,
    rightHeaderPage: PropTypes.string
}

//- default props
Header.defaultProps = {
    title: 'Home',
    welcomeText: 'Welcome, ',
    userName: 'Allessandro',
    dateTimeText: moment().format('MMMM Do, YYYY'),
    isDisplayDateTimeText: true,
    isDisplayWelcomeText: true,
    titleFontSize: 36,
    isShowAvatar: true,
    isMarginLeft: false,
    isMarginTop: false,
    isMarginBottom: false,
    isShowRightHeaderTitle: false
}