import React, { Component } from 'react';
import { 
    View, 
    Text,
    StyleSheet,
    Dimensions,
    TouchableOpacity,
    Alert,
    Animated,
    Modal, 
    TouchableHighlight
} from 'react-native';
import _ from 'lodash'
import moment from 'moment';

const { screenWidth, screenHeight } = Dimensions.get('window');

class SingleSchedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showDetail: false
        };
    }

    _onViewMorePress = (day) => 
    {
        // Alert.alert(
        //     'Alert Title',
        //     day,
        //     [
        //         {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
        //         {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        //         {text: 'OK', onPress: () => console.log('OK Pressed')},
        //     ],
        //     { cancelable: false }
        // )
        this.setState({showDetail: true});
    }

    _setModalVisible = (value) => 
    {
        this.setState({showDetail: value});
    }

    _timeConvert = (data) => {
        var minutes = data % 60;
        var hours = (data - minutes) / 60;
        
        return hours + " tiếng " + minutes + " phút";
    }

    _convertToHumanize = (minutes) => {
        console.log('check if < 60', parseInt(minutes) > 60)
        return (parseInt(minutes) > 60) ? this._timeConvert(minutes) : minutes + ' phút';
    }

    render() {
        const {
            data
        } = this.props;
        let day = moment(data.lDate).format('DD');
        let month = moment(data.lDate).format('MM');
        let year = moment(data.lDate).format('YYYY');
        return (
            <View style={styles.container}>
                <View style={styles.left}>
                    <Text style={styles.leftDateNumber}> {day} </Text>
                </View>
                <View style={styles.right}>
                    <View style={styles.content}>
                        <View style={styles.up}>
                            <Text style={styles.title}> {data.title} </Text>
                            <Text style={styles.subTitle}> {(data.isLate) ? 'Đến trễ ' + this._convertToHumanize(data.lLateMinutes) : 'Đi làm đúng giờ' } </Text>
                            <Text style={styles.subTitle}> {(data.isEarly) ? 'Ra về sớm ' + this._convertToHumanize(data.lEarlyMinutes) : 'Ra về đúng giờ' } </Text>
                        </View>
                        <TouchableOpacity style={styles.down}
                            onPress={() => this._onViewMorePress(day)}
                        >
                            <Text style={styles.viewMore}> Xem chi tiết </Text>
                        </TouchableOpacity>
                    </View>
                </View>

                {/* modal */}
                {(this.state.showDetail) &&
                    <View style={{
                        height: 100,
                        marginTop: 22
                    }}>
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={this.state.showDetail}
                            onRequestClose={() => {
                                Alert.alert('Modal has been closed.');
                            }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-end',
                                alignItems: 'center'
                            }}>
                                <View style={styles.modal}>

                                    {/* Close button area */}
                                    <View style={styles.modalCloseButton}>
                                        <TouchableHighlight
                                            style={{
                                                width: 100,
                                                height: 30,
                                                justifyContent: 'center',
                                                alignItems: 'flex-end'
                                            }}
                                            onPress={() => this._setModalVisible(!this.state.showDetail)}
                                        >
                                            <Text>Close</Text>
                                        </TouchableHighlight>
                                    </View>

                                    {/* Modal content area */}
                                    <View style={styles.modalContent}>
                                        <Text>This is modal content</Text>
                                    </View>
                                    
                                </View>
                            </View>
                        </Modal>
                    </View>
                }

            </View>
        );
    }
}

export default SingleSchedule;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: screenWidth,
        flexDirection: 'row',
        paddingTop: 25,
        paddingRight: 15,
    },
    left: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    right: {
        flex: 3,
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    content: {
        flex: 1,
        width: '100%',
        flexDirection: 'column',
        backgroundColor: 'white',
        alignItems: 'center',
        paddingTop: 10,
        // paddingBottom: 30,
        paddingLeft: 5,
        paddingRight: 5
    },
    title: {
        fontSize: 25,
        alignSelf: 'center',
    },
    subTitle: {
        fontSize: 18,
        alignSelf: 'flex-start',
    },
    viewMore: {
        fontSize: 18,
        alignSelf: 'flex-end',
    },
    leftDateNumber: {
        fontSize: 30
    },
    up:{
        flex: 2,
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: '#f9f9f9',
        paddingBottom: 10
    },
    down: {
        flex: 1,
        paddingTop: 15,
        paddingBottom: 15
    },
    modal: {
        flexDirection: 'column',
        width: '100%',
        height: 300,
        backgroundColor: 'white',
    },
    modalCloseButton: {
        flex: 0.25,
        alignItems: 'flex-end'
    },
    modalContent:{
        flex: 3
    }
})
