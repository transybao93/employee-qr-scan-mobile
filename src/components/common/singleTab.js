import React, { Component } from 'react';
import { 
    View, 
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import PropTypes from 'prop-types';

//- margin
import {
    element
} from '../../utils/margin';

//- tabSize
import {
  w,
  h
} from '../../utils/tabSize';

//- font
import {
  h7Font
} from '../../utils/fonts';

//- scale
import {
  scaleSize,
  ratio,
} from '../../utils/scale';

class SingleTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ratio: Math.min(Dimensions.get('window').height)
    }
  }

  render() {
    const {
      isMarginLeft,
      left,
      isMarginRight,
      right,
      tabName,
      imageLink,
      goTo
    } = this.props;

    let newLink = "";

    if(imageLink === "checkIn")
    {
      newLink = require('../../assets/images/tab/checkIn.png');
    }

    if(imageLink === "checkOut")
    {
      newLink = require('../../assets/images/tab/checkOut.png');
    }

    if(imageLink === "startLunch")
    {
      newLink = require('../../assets/images/tab/startLunch.png');
    }

    if(imageLink === "finishLunch")
    {
      newLink = require('../../assets/images/tab/finishLunch.png');
    }

    return (
      <TouchableOpacity
       style={[styles.container, {
         marginLeft: (isMarginLeft) ? left : 0,
         marginRight: (isMarginRight) ? right : 0
        }]} 
       onPress={() => (this.props.navigation !== undefined) ? this.props.navigation.navigate(goTo, {type: imageLink}): console.log('go to nowhere')}>
      
        
          <View style={styles.image}>
            <Image
              source={newLink}
            />
          </View>
          <View style={styles.box}>
              <Text>
                <Text style={styles.text}>{tabName}  </Text> 
                <Image
                  source={require('../../assets/images/tab/arrowNext.png')}
                />
              </Text>
          </View>
        

      </TouchableOpacity>
    );
  }
}

export default SingleTab;

const styles = StyleSheet.create({

  container: {
    // width: scaleSize(w),
    // height: scaleSize(h),

    width: w,
    height: h,
    aspectRatio: w / h,

    marginTop: element,
    flex: 1,
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
    // borderWidth: 0.25,
    // box shadow
    //- ios
    shadowOpacity: 2,
    shadowRadius: 4,
    shadowColor: 'grey',
    shadowOffset: { height: 0, width: 0 },
    //- android
    elevation: 2,
  },

  image: {
    flex: 150,
    // width: '100%',
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00224B',
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },

  box: {
    flex: 50,
    // width: '100%',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
    borderBottomWidth: 0.25,
    borderRightWidth: 0.25,
    borderLeftWidth: 0.25,
    borderColor: 'grey',
  },

  text: {
    fontFamily: h7Font,
    fontSize: 14,
  },

});

//- prop types
SingleTab.propTypes = {
  isMarginLeft: PropTypes.bool,
  isMarginRight: PropTypes.bool,
  left: PropTypes.number,
  right: PropTypes.number,
  tabName: PropTypes.string,
  imageLink: PropTypes.string,
}
//- default
SingleTab.defaultProps = {
  isMarginLeft: false,
  isMarginRight: false
}
