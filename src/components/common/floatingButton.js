import React, { Component } from 'react';
import { 
    View, 
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

class FloatingButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    _onButtonPress = () => {
        console.log('Scan button pressed...')
        this.props.navigation.navigate('QR');
    }

    render() {
        return (
        <TouchableOpacity 
            style={styles.container}
            onPress={this._onButtonPress}
        >
            <Text> Scan </Text>
        </TouchableOpacity>
        );
    }
}

export default FloatingButton;
const styles = StyleSheet.create({

    container: {
        width: 60,  
        height: 60,   
        borderRadius: 30,            
        backgroundColor: '#ee6e73',                                    
        position: 'absolute',                                          
        bottom: 20,                                                    
        right: 20, 
        alignItems: 'center',
        justifyContent: 'center'
    }

})