import React, { Component } from 'react'
import { 
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    AsyncStorage,
    Keyboard,
    Alert
} from 'react-native'
import { 
    FormInput,
    Button
} from 'react-native-elements'
import { 
    BUTTON_MAIN_COLOR,
    LEFT_RIGHT_MARGIN
} from "../../helper/constant";

//- import font
import {
    body1
} from '../../utils/fonts';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {
    login
} from '../../redux/actions/userActions';
import _ from 'lodash'

export class input extends Component {

    constructor(props){
        super(props)
        this.state = {
            text: "hehe",
            email: '',
            password: '',
            token: '',
            loading: false,
            disable: false,
        }
    }

    _onLoginButtonPress = async () => {
        //- start loading
        this.setState({
            loading: true,
            disable: true,
        });
        let email = this.state.email;
        let password = this.state.password;
        await this.props.loginAction(email, password);
        let {
            token 
        } = this.props.token;
        if(_.size(token) > 0)
        {
            //- save token to async storage
            await AsyncStorage.setItem('userToken', token);
            //- hide the keyboard
            Keyboard.dismiss();
            //- redirect to main page
            this.props.navigation.navigate('Main');
            this.setState({
                loading: false,
                disable: false,
            })
        }else{
            //- end loading
            this.setState({
                loading: false,
                disable: false,
            })
            //- show alert
            Alert.alert(
                'Error...',
                'Your account is not exist or your password is wrong. Please try again!',
                [
                    {text: 'Dismiss', onPress: () => console.log('OK Pressed')},
                ],
                { cancelable: false }
            )
        }
    }

    render() {
        return (
            <View style={styles.container}>

                <FormInput 
                    placeholder={'Email'}
                    containerStyle={styles.containerStyle}
                    inputStyle={styles.inputStyle}
                    onChangeText={(text) => this.setState({
                        email: text
                    })}
                />
                <FormInput 
                    placeholder={'Password'}
                    containerStyle={styles.containerStyle}
                    inputStyle={styles.inputStyle}
                    onChangeText={(text) => this.setState({
                        password: text
                    })}
                    secureTextEntry={true}
                />
                <Button 
                    title="Sign in"
                    loading={this.state.loading}
                    disabled={this.state.disable}
                    borderRadius={6}
                    backgroundColor={BUTTON_MAIN_COLOR}
                    buttonStyle={styles.buttonStyle}
                    textStyle={styles.textStyle}
                    onPress={this._onLoginButtonPress}
                />
                <TouchableOpacity style={styles.recoverPass}>
                    <Text style={styles.recoverPassText}>
                        Recover password?
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

// export default input

//- redux
const mapStateToProps = (state) => ({
	token: (_.size(state.userReducer.data) > 0 && state.userReducer.data !== null) ? state.userReducer.data.data : 0,
})
  
const mapDispatchToProps = (dispatch) => {
	return {
		// logoutAction: bindActionCreators((token) => logout(token), dispatch),
		loginAction: bindActionCreators((email, password) => login(email, password), dispatch),
	}
}
  
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(input)

const styles = StyleSheet.create({

    container:{
        flex: 1,
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        marginLeft: LEFT_RIGHT_MARGIN,
        marginRight: LEFT_RIGHT_MARGIN,
    },

    containerStyle: {
        borderWidth: 0.25,
        marginTop: 10,
        borderRadius: 6,
        borderColor: '#00224B',
    },

    buttonStyle:{
        marginTop: 10,
    },

    inputStyle: {
        fontSize: 14,
        paddingLeft: 10
    },

    textStyle:{
        fontSize: 16
    },

    recoverPass: {
        marginTop: 6,
        alignItems: 'center',
    },
    recoverPassText: {
        fontFamily: body1,
        fontSize: 14
    },
})