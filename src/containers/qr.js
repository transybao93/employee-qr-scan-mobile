import React, { Component } from 'react'
import { 
    StyleSheet, 
    Text,
    AsyncStorage
} from 'react-native'
import QRCodeScanner from 'react-native-qrcode-scanner';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {
    scan
} from '../redux/actions/userActions';
import moment from 'moment';
import _ from 'lodash';
import {
	viewWeekStatistic
} from '../redux/actions/userActions';

export class QR extends Component {

    constructor(props)
    {
        super(props);
        this.state = {
            data: null,
            statusText: 'Tìm kiếm mã QRCOde...'
        }
    }

    //- check if variable is already have data

    _onSuccess = async (e) => {
        console.log('scan data', e)

        // this.setState({
        //     data: e.data
        // })
        //- get current date, time, month
        let params = {
            lDate: moment().format('YYYY-MM-DD'),
            lTime: moment().format('HH:mm:ss'),
            lMonth: moment().format('MM')
        }
        await this.props.scanAction(params);
        this.setState({
            statusText: 'Quét mã thành công !'
        })
        //- fire an action to get week statistic
        await this._getWeekStatistic();
        console.log('week list in statistic', this.props.weekList)
        this.props.navigation.navigate('Main');
    }

    _getWeekStatistic = async () => {
        //- fire an action to get week statistic
        let currentDate = moment().format('YYYY-MM-DD');
		await this.props.viewWeekStatistic(currentDate);
		console.log('view week statistic', this.props.responseData)
		let responseData = this.props.responseData;
		if(responseData !== 0)
		{
			//- has data
			console.log('is not equal to 0...')
			this.setState({
				weekList: responseData
			});
		}
	}

    render() {
        return (

            <QRCodeScanner
                reactivateTimeout={5000}
                showMarker={true}
                reactivate={false}
                onRead={this._onSuccess}
                topContent={
                <Text style={styles.centerText}>
                    Di chuyển điện thoại qua mã QRCode để tiến hành điểm danh
                </Text>
                }
                bottomContent={
                    <Text> {this.state.statusText} </Text>
                }
            />

        )
    }
}

// export default QR

//- redux
const mapStateToProps = (state) => ({
	weekList: (_.size(state.userReducer.weekList) > 0 && state.userReducer.weekList !== null) ? state.userReducer.weekList.data : 0,
})
  
const mapDispatchToProps = (dispatch) => {
	return {
		// logoutAction: bindActionCreators((token) => logout(token), dispatch),
        scanAction: bindActionCreators((params) => scan(params), dispatch),
        viewWeekStatistic: bindActionCreators((chosenDate) => viewWeekStatistic(chosenDate), dispatch),
	}
}
  
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(QR)

const styles = StyleSheet.create({
    centerText: {
        flex: 1,
        fontSize: 18,
        padding: 32,
        color: '#777',
    },
    textBold: {
        fontWeight: '500',
        color: '#000',
    },
    buttonText: {
        fontSize: 21,
        color: 'rgb(0,122,255)',
    },
    buttonTouchable: {
        padding: 16,
    },
});
