import React, { Component } from 'react';
import { 
    View, 
    Text,
    StyleSheet,
    FlatList,
} from 'react-native';
import SingleListItem from '../components/common/singleListItem';
import BasicHeader from '../components/common/basicHeader';

class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <BasicHeader 
                    navigation={this.props.navigation}
                    headerTitle={'Thông báo'}
                    haveBackButton={false}
                />
                <View style={styles.noti}>
                    <FlatList
                        data={[
                            {key: 'a'}, 
                            {key: 'b'}
                        ]}
                        renderItem={({item}) => 
                            <SingleListItem
                                item={item}
                            />
                        }
                    />
                </View>
            </View>
        );
    }
}

export default Notification;

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
    },
    noti: {
        flex: 4,
        marginTop: 20,
        marginRight: 20,
        marginLeft: 20,
    },

});