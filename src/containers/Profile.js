import React, { Component } from 'react'
import { 
  View,
  StyleSheet,
  ScrollView,
  Alert,
  AsyncStorage
} from 'react-native';
import { connect } from 'react-redux';
import { List, ListItem } from 'react-native-elements'
import { bindActionCreators } from 'redux'
import _ from 'lodash';

const list = [
	{
		name: 'Email',
		rightTitle: 'bao@gmail.com',
		hideChevron: false,
		disabled: false,
	},
	{
		name: 'Phone number',
		rightTitle: '0123 456 789',
		hideChevron: false,
		disabled: false,
	},
	{
		name: 'Password',
		rightTitle: '*************',
		hideChevron: false,
		disabled: false,
	},
	{
		name: 'Sign out',
		rightTitle: null,
		hideChevron: false,
		disabled: false,
	}
]


//- header
import Header from '../components/common/header';

//- image area
import ImageArea from '../components/common/imageArea';

//- margin
import {
  left,
  right
} from '../utils/margin';

//- import color
import {
  bgColor
} from '../utils/color';

export class Profile extends Component {

	_signOut = async () => {
		//- remove key in Async Storage
		await AsyncStorage.removeItem('userToken');
		//- navigate to login page
		this.props.navigation.navigate('Login');
	}

	render() {
		return (
		<ScrollView style={styles.container}>

			{/* Header */}
			<Header
			isShowAvatar={false}
			title={'Profile'}
			isMarginLeft={true}
			left={left}
			isMarginBottom={true}
			bottom={20}
			isDisplayWelcomeText={false}
			isDisplayDateTimeText={false}
			/>

			{/* Image area */}
			<View style={styles.imageArea}>
				<ImageArea/>
			</View>

			{/* list */}
			<View style={styles.infoList}>
			{/* list item */}
			<List containerStyle={{borderTopWidth: 0, marginTop: 0}}>
				{
				list.map((l) => (
					<ListItem
					rightTitle={l.rightTitle}
					key={l.name}
					title={l.name}
					iconRight={false}
					disabled={l.disabled}
					hideChevron={l.hideChevron}
					onPress={() => {
						if(l.name === "Sign out")
						{
							Alert.alert(
								'Precaution !',
								'Do you want to logout?',
								[
									{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
									{text: 'OK', onPress: async () => {
										//- remove key in Async Storage
										await AsyncStorage.removeItem('userToken');
										//- navigate to login page
										this.props.navigation.navigate('Login');
									}},
								],
								{ cancelable: false }
							)
						}else{
						//- navigate to edit profile
						//- send with profile and id
						this.props.navigation.navigate('EditProfile', {
							userID: 1,
							text: l.rightTitle,
							type: l.name
						});
						}
					}}
					/>
				))
				}
			</List>

			</View>

		</ScrollView>
		)
	}
}

// export default Profile

//- redux
const mapStateToProps = (state) => ({
	data: (_.size(state.userReducer.data) > 0 && state.userReducer.data !== null) ? state.userReducer.data.data : 0,
})
  
const mapDispatchToProps = (dispatch) => {
	return {
		logoutAction: bindActionCreators((token) => logout(token), dispatch),
		getUserInfo: bindActionCreators((token) => getUserInfo(token), dispatch)
	}
}
  
export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Profile)


const styles = StyleSheet.create({

	container: {
		flex: 1,
		// paddingLeft: left,
		// paddingRight: right,
		backgroundColor: bgColor,
	},

	imageArea:{
		flex: 0.5
	},

	infoList: {
		flex: 1
	}


});