import React, { Component } from 'react'
import { 
  Text, 
  View ,
  Button,
  StyleSheet
} from 'react-native'

export class RecoverPass extends Component {

  _onPress = () => {
    const { navigate } = this.props.navigation;
    navigate('Home');
  }

  render() {
    return (
      <View>
        <Text> This is recover pass page </Text>
        <Button onPress={this._onPress} title="Go to Home tab page"/>
      </View>
    )
  }
}

export default RecoverPass