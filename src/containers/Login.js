import React, { Component } from 'react'
import { 
	View,
	StyleSheet,
	Image,
	AsyncStorage,
	ActivityIndicator
} from 'react-native';
import Input from '../components/Login/input'
import {
	login
} from '../redux/actions/userActions';

//- color
import {
  	bgColor
} from '../utils/color';

export class Login extends Component {

	constructor(props)
	{
		super(props);
		this.state = {
			isLoading: true,
		}
		//- check token
		this._checkToken();
	}

	//- check token from AsyncStorage
	_checkToken = async () => {
		const userToken = await AsyncStorage.getItem('userToken');
		console.log('user token', userToken)
		if (userToken !== null) {

			//- check if token is valid and not expired

			//- token exist
			this.props.navigation.navigate("Main");
			this.setState({
				isLoading: false
			})
		}else{
			this.setState({
				isLoading: false
			})
		}
	}

	_onPress = () => {
		const { navigate } = this.props.navigation;
		navigate('RecoverPass');
	}

	render() {
		
		return (
			<View style={styles.container}>

				<View style={styles.logo}>
					<Image source={require('../assets/images/logo.png')} />
				</View>

				<View style={styles.input}>

					{(this.state.isLoading) 
					?
						<ActivityIndicator size="large" color="#0000ff" />
					:
						<Input
							navigation={this.props.navigation}
						/>
					}

				</View>


			</View>
		)
	}
}

export default Login

const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: bgColor
  },

  logo: {
    flex: 2,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  input: {
    flex: 3.5,
    justifyContent: 'center',
    alignItems: 'center',
  },

})