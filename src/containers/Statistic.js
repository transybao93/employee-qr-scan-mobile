import React, { Component } from 'react'
import { 
	View,
	StyleSheet,
	FlatList,
	StatusBar,
	BackHandler,
	Picker,
	Image,
	Text
} from 'react-native'

//- font
import {
  h5Font,
  body4
} from '../utils/fonts';
import SingleListItem from '../components/common/singleListItem';
import BasicHeader from '../components/common/basicHeader';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {
	viewMonthStatistic,
	viewMonthLogtime
} from '../redux/actions/userActions';
import _ from 'lodash';
import moment from 'moment';

export class Statistic extends Component {

	constructor(props) {
        super(props);
        this.state = {
			currentMonth: '1'
        };
	}

	componentDidMount() {
		StatusBar.setHidden(true);
		BackHandler.addEventListener('hardwareBackPress', this._handleBackButton);
		this._getMonthStatistic();
	}
	
	_getMonthStatistic = async () => {
		//- default is view month before
		let monthBefore = moment().subtract(1, 'months').format('MM');
		this.setState({
			currentMonth: monthBefore
		})
		await this.props.viewMonthStatistic(monthBefore);
		await this.props.viewMonthLogtime(monthBefore);
		console.log('view month statistic', this.props.monthList)
		console.log('view month logtime', this.props.monthLogtime)
	}

	_onMonthPress = async (month) => {
		console.log('selected month', month)
		this.setState({currentMonth: month})
		//- fire an action
		await this.props.viewMonthStatistic(month);
		await this.props.viewMonthLogtime(month);
		console.log('view month statistic when change month', this.props.monthList)
		console.log('view month logtime', this.props.monthLogtime)
	}

	_timeConvert = (data) => {
        var minutes = data % 60;
        var hours = (data - minutes) / 60;
        
        return hours + " tiếng " + minutes + " phút";
    }

    render() {
		let monthList = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
		const months = monthList.map((monthNumber, key) => {
			return (
				<Picker.Item label={'tháng ' + monthNumber} value={monthNumber} key={key}/>
			)
		})
		const totalLateMinutes = _.pick(this.props.monthList[0], 'totalLateMinutes').totalLateMinutes
		const totalEarlyMinutes = _.pick(this.props.monthList[0], 'totalEarlyMinutes').totalEarlyMinutes
        return (
            <View style={styles.container}>
                <BasicHeader 
                    navigation={this.props.navigation}
                    headerTitle={'Thống kê'}
                    haveBackButton={false}
                />
				<View style={styles.noti}>
					<Picker
						selectedValue={this.state.currentMonth}
						style={styles.monthPicker}
						onValueChange={(itemValue, itemIndex) => this._onMonthPress(itemValue)}>
						{months}
					</Picker>
					
					<View style={styles.totalArea}>
						<View style={styles.totalContent}>
							<Text> Bạn đang chọn tháng {this.state.currentMonth} </Text>
							<Text> Tổng số thời gian đi làm trễ: {(totalLateMinutes !== undefined) ? (totalLateMinutes > 60) ? this._timeConvert(totalLateMinutes): totalLateMinutes + ' phút' : '(Chưa có thống kê)'} </Text>    
							<Text> Tổng số thời gian đi về sớm: {(totalEarlyMinutes !== undefined) ? (totalEarlyMinutes > 60) ? this._timeConvert(totalEarlyMinutes): totalEarlyMinutes + ' phút' : '(Chưa có thống kê)'} </Text>    
						</View>
					</View>

                    <FlatList
                        data={this.props.monthLogtime}
                        renderItem={({item}) => 
                            <SingleListItem
                                item={item}
                            />
                        }
                    />
                </View>
            </View>
        );
    }
}

// export default Statistic

//- redux
const mapStateToProps = (state) => ({
	monthList: (_.size(state.userReducer.monthList) > 0 && state.userReducer.monthList !== null) ? state.userReducer.monthList.data.data : [],
	monthLogtime: (_.size(state.userReducer.monthLogtime) > 0 && state.userReducer.monthLogtime !== null) ? state.userReducer.monthLogtime.data.data : [],
})
  
const mapDispatchToProps = (dispatch) => {
	return {
		viewMonthStatistic: bindActionCreators((chosenMonth) => viewMonthStatistic(chosenMonth), dispatch),
		viewMonthLogtime: bindActionCreators((chosenMonth) => viewMonthLogtime(chosenMonth), dispatch),
	}
}
  
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Statistic)

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
    },
    noti: {
        flex: 4,
        marginTop: 20,
        marginRight: 20,
        marginLeft: 20,
	},
	monthPicker: {
		height: 25,
		width: 'auto',
		marginBottom: 15
	},

	// total area
	totalArea: {
        // flex: 1,
        // flexDirection: 'row',
        marginBottom: 20,
        backgroundColor: 'white',
        // paddingTop: 10,
		// paddingBottom: 10,
		height: 80,
    },
    totalContent: {
		flex: 1,
		paddingTop: 10,
		paddingBottom: 10,
    }

});