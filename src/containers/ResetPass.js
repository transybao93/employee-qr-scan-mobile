import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet,
    TextInput
} from 'react-native'
import {
    left,
    right,
    element
} from '../utils/margin';

export class ResetPass extends Component {

    constructor(props)
    {
        super(props);
        this.state = {
            text: ''
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>This is reset password page</Text>
                <TextInput
                    onChangeText={(text) => this.setState({text})}
                    value={this.state.text}
                />
            </View>
        )
    }
}

export default ResetPass

const styles = StyleSheet.create({

    container: {
        flex: 1,
        paddingLeft: left,
        paddingRight: right,
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
    }

});
