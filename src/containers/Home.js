import React, { Component } from 'react'
import { 
	Text, 
	View,
	StyleSheet,
	StatusBar,
	AsyncStorage,
	BackHandler
} from 'react-native'
import { 
	MAIN_COLOR
} from "../helper/constant";

//- import color
import {
	bgColor
} from '../utils/color';

//- margin
import {
	left,
	right
} from '../utils/margin';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import {
	viewWeekStatistic
} from '../redux/actions/userActions';
import _ from 'lodash';
import moment from 'moment';

//- schedule
import { Agenda } from 'react-native-calendars';
import SingleSchedule from '../components/common/singleSchedule';
import FloatingButton from '../components/common/floatingButton';

let testItems = {
	'2018-12-24': [
		{
			day: '24',
			month: '12',
			year: '2018',
			title: 'tick xanh',
			in: '8:29 AM',
			out: '5:35 PM'
		}
	],
	'2018-12-25': [
		{
			day: '25',
			month: '12',
			year: '2018',
			title: 'tick xanh',
			in: '8:29 AM',
			out: '5:35 PM'
		}
	],
	'2018-12-26': [
		{
			day: '26',
			month: '12',
			year: '2018',
			title: 'tick xanh',
			in: '8:29 AM',
			out: '5:35 PM'
		}
	],
	'2018-12-27': [
		{
			day: '27',
			month: '12',
			year: '2018',
			title: 'tick xanh',
			in: '8:29 AM',
			out: '5:35 PM'
		}
	],
	'2018-12-28': [],
	'2018-12-29': [
		{
			day: '29',
			month: '12',
			year: '2018',
			title: 'tick đỏ',
			in: '8:45 AM', //- trễ 15 phút
			out: '5:35 PM'
		}
	],
	'2018-12-30': [
		{
			day: '30',
			month: '12',
			year: '2018',
			title: 'tick đỏ',
			in: '8:25 AM', 
			out: '5:15 PM' //- ra về sớm 15 phút
		}
	],
}

export class Home extends Component {

	constructor(props)
	{
		super(props);
		this.state = {
			weekList: this.props.weekList
		}
	}

	componentDidMount() {
		StatusBar.setHidden(true);
		BackHandler.addEventListener('hardwareBackPress', this._handleBackButton);
		this._getWeekStatistic();
	}

	_getWeekStatistic = async () => {
		//- fire an action to get week statistic
		let currentDate = moment().format('YYYY-MM-DD');
		console.log('current date', currentDate)
		await this.props.viewWeekStatistic(currentDate);
		console.log('view week statistic', this.props.weekList)
		if(this.props.weekList !== 0 || this.props.weekList !== undefined)
		{
			//- has data
			this.setState({
				weekList: this.props.weekList
			});
		}
	}

	componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this._handleBackButton);
    }

	_handleBackButton() {
        return true;
    }

	render() {
		const working = {key: 'working', color: 'green', selectedDotColor: 'white'};
		const off = {key: 'off', color: 'red', selectedDotColor: 'white'};
		
		console.log('week list from state', this.state.weekList);

		//- convert some data from weekList to show dots
		let dots = _.keys(this.state.weekList.data);
		let dots2 = _.zipObject(dots, _.map(dots, _.constant({dots: [working]})));
		console.log('dots data....', dots2)

		return (
			<View style={styles.container}>
				
				<Agenda
					// the list of items that have to be displayed in agenda. If you want to render item as empty date
					// the value of date key kas to be an empty array []. If there exists no value for date key it is
					// considered that the date in question is not yet loaded
					// items={this.state.weekList.data}
					items={null}
					// callback that gets called when items for a certain month should be loaded (month became visible)
					// loadItemsForMonth={(month) => {console.log('trigger items loading')}}
					// callback that fires when the calendar is opened or closed
					// onCalendarToggled={(calendarOpened) => {console.log(calendarOpened)}}
					// callback that gets called on day press
					onDayPress={(day)=>{console.log('day pressed')}}
					// callback that gets called when day changes while scrolling agenda list
					onDayChange={(day)=>{console.log('day changed')}}

					// initially selected day
					selected={moment().format('YYYY-MM-DD')}

					// Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
					minDate={moment().startOf('isoWeek').format('YYYY-MM-DD')}
					// Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
					maxDate={moment().endOf('isoWeek').format('YYYY-MM-DD')}
					// Max amount of months allowed to scroll to the past. Default = 50
					pastScrollRange={2}
					// Max amount of months allowed to scroll to the future. Default = 50
					futureScrollRange={1}
					// specify how each item should be rendered in agenda
					renderItem={(item, firstItemInDay) => {return (
						<SingleSchedule
							data={item}
						/>
					);}}
					// specify how each date should be rendered. day can be undefined if the item is not first in that day.
					renderDay={(day, item) => {return (<View/>);}}
					// specify how empty date content with no items should be rendered
					renderEmptyDate={() => {return (<Text>No data for this day</Text>);}}
					renderEmptyData = {() => {return (<Text>No data for this day</Text>);}}
					// specify your item comparison function for increased performance
					rowHasChanged={(r1, r2) => {return r1.text !== r2.text}}
					// Hide knob button. Default = false
					// renderKnob={() => {return (<Text>View more</Text>);}}
					hideKnob={false}
					firstDay={1}
					markingType={'multi-dot'}
					// By default, agenda dates are marked if they have at least one item, but you can override this if needed
					// markedDates={{
					// 	'2018-12-24': {dots: [working]},
					// 	'2018-12-25': {dots: [working]},
					// 	'2018-12-26': {dots: [working]},
					// 	'2018-12-27': {dots: [working]},
					// 	'2018-12-28': {dots: [off]},
					// 	'2018-12-29': {dots: [working]},
					// 	'2018-12-30': {dots: [working]},
					// }}
					markedDates={dots2}
					// agenda theme
					theme={{
						agendaDayTextColor: 'yellow',
						agendaDayNumColor: 'green',
						agendaTodayColor: 'red',
						agendaKnobColor: 'blue'
					}}
				/>
				<FloatingButton
					navigation={this.props.navigation}
				/>
			</View>
		)
	}
}

// export default Home

//- redux
const mapStateToProps = (state) => ({
	weekList: (_.size(state.userReducer.weekList) > 0 && state.userReducer.weekList !== null) ? state.userReducer.weekList.data : [],
})
  
const mapDispatchToProps = (dispatch) => {
	return {
		// logoutAction: bindActionCreators((token) => logout(token), dispatch),
		viewWeekStatistic: bindActionCreators((chosenDate) => viewWeekStatistic(chosenDate), dispatch),
	}
}
  
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home)

const styles = StyleSheet.create({

	container: {
		flex: 1,
	}

})