import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet,
    TextInput,
    Alert
} from 'react-native'
import _ from 'lodash';
import BasicHeader from '../components/common/basicHeader';

//- import color
import {
    bgColor
} from '../utils/color';
  
//- margin
import {
    left,
    right
} from '../utils/margin';
import {
    update
} from '../redux/actions/userActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

export class EditProfile extends Component {

    constructor(props) {
        super(props)
        this.state = { 
            params: this.props.navigation.state.params,
            text: this.props.navigation.state.params.text,
            id: this.props.navigation.state.params.userID,
            inputName: this.props.navigation.state.params.type,
            oldPasswordPlaceholder: 'Your current password',
            newPasswordPlaceholder: 'Your new password',
            confirmPasswordPlaceholder: 'Confirm your password'
        };
    }

    _onButtonPress = async () => {

        //- check if text or password
        if(this.state.inputName !== "Password")
        {
            console.log('not password edit')
            this.setState({
                text: ''
            })
        }else{
            console.log('password edit')
            console.log('old password', this.state.oldPasswordPlaceholder)
            console.log('new password', this.state.newPasswordPlaceholder)
            console.log('new password confirm', this.state.confirmPasswordPlaceholder)
            let {
                oldPasswordPlaceholder,
                newPasswordPlaceholder,
                confirmPasswordPlaceholder
            } = this.state;
            if(confirmPasswordPlaceholder === newPasswordPlaceholder)
            {
                //- if confirm password === new password
                await this.props.updateAction({
                    ePassword: newPasswordPlaceholder,
                    eOldPassword: oldPasswordPlaceholder
                });
                //- update and response to previous page
                if(this.props.response !== 0)
                {
                    //- update success
                    Alert.alert(
                        'Success',
                        'Update information success !',
                        [
                            {text: 'OK', onPress: () => this.props.navigation.goBack()},
                        ],
                        { cancelable: false }
                    )
                }
                //- show some errors
                console.log('response data when edit profile', this.props.response)

            }

        }
    }

    render() {
        return (
        <View style={styles.container}>
            <BasicHeader
                headerTitle={'Edit profile'}
                navigation={this.props.navigation}
                haveRightButton={true}
                rightButtonText={'Save'}
                rightButtonOnPress={this._onButtonPress}
                haveBackButton={true}
            />
            

            {(this.state.inputName !== "Password") &&
                <View style={styles.content}>
                    <TextInput
                        style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                        onChangeText={(text) => this.setState({text})}
                        value={this.state.text}
                        keyboardType={(this.state.inputName === "Phone number") ? 'phone-pad' : 'default'}
                        returnKeyType={'done'}
                        enablesReturnKeyAutomatically={true}
                        clearButtonMode={'unless-editing'}
                    />
                </View>
            }

            {(this.state.inputName === "Password") &&
                <View style={styles.content}>
                    <TextInput
                        style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                        onChangeText={(text) => this.setState({
                            oldPasswordPlaceholder: text
                        })}
                        // value={this.state.oldPasswordPlaceholder}
                        placeholder={this.state.oldPasswordPlaceholder}
                        returnKeyType={'next'}
                        //- go to next text input
                        onSubmitEditing={() => { this.newPass.focus(); }}
                        //- secure
                        secureTextEntry={true}
                        enablesReturnKeyAutomatically={true}
                    />
                    <TextInput
                        style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                        onChangeText={(text) => this.setState({
                            newPasswordPlaceholder: text
                        })}
                        // value={this.state.newPasswordPlaceholder}
                        placeholder={this.state.newPasswordPlaceholder}
                        returnKeyType={'next'}
                        //- when click next
                        onSubmitEditing={() => { this.confirmPass.focus(); }}
                        ref={(input) => { this.newPass = input; }}
                        //- secure
                        secureTextEntry={true}
                        enablesReturnKeyAutomatically={true}
                    />
                    <TextInput
                        style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                        onChangeText={(text) => this.setState({
                            confirmPasswordPlaceholder: text
                        })}
                        // value={this.state.confirmPasswordPlaceholder}
                        placeholder={this.state.confirmPasswordPlaceholder}
                        //- when done
                        returnKeyType={'done'}
                        ref={(input) => { this.confirmPass = input; }}
                        //- secure
                        secureTextEntry={true}
                        enablesReturnKeyAutomatically={true}
                    />
                </View>
            }

            
        </View>
        )
    }
}

// export default EditProfile


//- redux
const mapStateToProps = (state) => ({
	response: (_.size(state.userReducer.data) > 0 && state.userReducer.data !== null) ? state.userReducer.data.data : 0,
})
  
const mapDispatchToProps = (dispatch) => {
	return {
        // logoutAction: bindActionCreators((token) => logout(token), dispatch),
        updateAction: bindActionCreators((params) => update(params), dispatch),
	}
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(EditProfile)

const styles = StyleSheet.create({

    container: {
        flex: 1,
        paddingLeft: left,
        paddingRight: right,
        backgroundColor: bgColor
    },
    content: {
        flex: 4
    }

});