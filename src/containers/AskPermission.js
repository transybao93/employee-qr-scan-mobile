import React, { Component } from 'react';
import { 
	View, 
	Text,
	StyleSheet,
	Image,
} from 'react-native';
import {
	BUTTON_MAIN_COLOR, 
	LEFT_RIGHT_MARGIN
} from '../helper/constant';
import { 
	Button
} from 'react-native-elements';

//- color
import {
	bgColor
} from '../utils/color';

class AskPermission extends Component {
	constructor(props) {
		super(props);
		this.state = {
		};
	}

	render() {
		return (
			<View style={styles.container}>
				<Image style={styles.pin} source={require('../assets/images/pin.png')} />
				<Text style={styles.textStyle}>
					Allow Oclerk to access this device’s location to countinue to use this application
				</Text>
				<Button 
					title="Settings"
					borderRadius={6}
					backgroundColor={BUTTON_MAIN_COLOR}
					buttonStyle={styles.buttonStyle}
					textStyle={styles.textStyle}
				/>
			</View>
		);
	}
}

export default AskPermission;

const styles = StyleSheet.create({

	container: {
		flex: 1,
		flexDirection: 'column',
		alignContent: 'center',
		justifyContent: 'center',
		backgroundColor: bgColor
	},

	pin:{
		alignSelf: 'center',
		marginBottom: 34
	},

	buttonStyle: {
		backgroundColor: BUTTON_MAIN_COLOR,
		marginTop: 10,
	},

	textStyle:{
		fontSize: 16,
		marginLeft: LEFT_RIGHT_MARGIN,
		marginRight: LEFT_RIGHT_MARGIN,
	},

})
