import React from 'react';
import {
Image,
Text,
} from 'react-native';

import { 
createBottomTabNavigator,
createStackNavigator
} from 'react-navigation';

//- Stack page
import Login from './containers/Login';
import RecoverPass from './containers/RecoverPass'; 
import ResetPass from './containers/ResetPass';

//- TabNavigator page
import Statistic from './containers/Statistic';
import Home from './containers/Home';
import Profile from './containers/Profile';
import Notification from './containers/Notification';

//- Other screen
import EditProfile from './containers/EditProfile';

//- qr code
import QR from './containers/qr'

//- import color
import {
	activeColor,
	inactiveColor
} from './utils/color';

//- customizable icon
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from './utils/icons/selection.json';
const CustomIcon = createIconSetFromIcoMoon(icoMoonConfig);

const StackNavigator = createStackNavigator({
	Login: {
		screen: Login,
			navigationOptions: ({ navigation }) => ({
			header: null,
		}),
	},
	QR: {
		screen: QR,
			navigationOptions: ({ navigation }) => ({
			header: null,
		}),
	},
	ResetPass: {
		screen: ResetPass,
			navigationOptions: ({ navigation }) => ({
			header: null,
		}),
	},
	EditProfile: {
		screen: EditProfile,
			navigationOptions: ({ navigation }) => ({
			header: null,
		}),
	},
	//- Tabbar Navigation
	Main: {
		screen: createBottomTabNavigator({
		Home: {
			screen: Home,
			navigationOptions: ({ navigation }) => ({
			title: 'Trang chủ',
			tabBarIcon: ({tintColor}) => 
				<CustomIcon
				name='home'
				size={22}
				color={tintColor}
				/>

			}),
		},
		Statistic: {
			screen: Statistic,
			navigationOptions: ({ navigation }) => ({
			title: 'Thống kê',
			tabBarIcon: ({tintColor}) => 
				<CustomIcon
				name='statistic'
				size={22}
				color={tintColor}
				/>
			}),
		},
		Notification: {
			screen: Notification,
			navigationOptions: ({ navigation }) => ({
			title: 'Thông báo',
			tabBarIcon: ({tintColor}) => 
				<CustomIcon
				name='notification'
				size={22}
				color={tintColor}
				/>
			}),
		},
		Profile: {
			screen: Profile,
			navigationOptions: ({ navigation }) => ({
			title: 'Cá nhân',
			tabBarIcon: ({tintColor}) => 
				<CustomIcon
				name='user'
				size={22}
				color={tintColor}
				/>
			}),
		},

		},
		{
		tabBarOptions: {
			activeTintColor: activeColor,
			inactiveTintColor: inactiveColor,
		},
		}
		),
		navigationOptions: ({ navigation }) => ({
		title: 'Home',
		order: ['Home', 'Statistic', 'Profile'],
		header: null
		}),
	},

	},
	{
	initialRouteName: 'Login'
	}

);

export default StackNavigator;
