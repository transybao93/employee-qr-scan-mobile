import _ from 'lodash';
import moment from 'moment';
import * as method from './method';

//- api for user

const login = async (params) => method.post('/emp/login', params);
// const update = async (id, params) => method.patch('/emp/update/' + id, params);
const update = async (params) => method.patch('/emp/update/by/token', params);
const scan = async (params) => method.post('/logtime/scan', params);
const viewWeekStatistic = async (chosenDate) => method.get('/statistic/history/by/week/' + chosenDate);
const viewMonthStatistic = async (chosenMonth) => method.get('/statistic/history/by/month/' + chosenMonth);
const viewMonthLogtime = async (chosenMonth) => method.get('/logtime/history/month/' + chosenMonth);

export {
    login,
    update,
    scan,
    viewWeekStatistic,
    viewMonthStatistic,
    viewMonthLogtime
}