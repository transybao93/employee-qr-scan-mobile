import * as basicActions from './index';
//- api
import * as api from '../../services/api';
import _ from 'lodash';
import moment from 'moment';

const login = (email, password) => {
    console.log('email', email)
    console.log('password', password)
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        //- call api
        return api.login({
            eEmail: email,
            ePassword: password
        })
        .then((response) => {
            if(_.size(response) > 0 && response !== undefined)
            {
                dispatch(basicActions.getDataSuccess(response));
            }else{
                dispatch(basicActions.getDataSuccess(null));
            }
        })
        .catch((err) => console.log('action error', err));

    }
}

const update = (params) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        //- call api
        return api.update(params)
        .then((response) => {
            if(_.size(response) > 0 && response !== undefined)
            {
                dispatch(basicActions.getDataSuccess(response));
            }else{
                dispatch(basicActions.getDataSuccess(null));
            }
        })
        .catch((err) => console.log('action error', err));

    }
}

const scan = (params) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        //- call api
        return api.scan(params)
        .then((response) => {
            if(_.size(response) > 0 && response !== undefined)
            {
                dispatch(basicActions.getDataSuccess(response));
            }else{
                dispatch(basicActions.getDataSuccess(null));
            }
        })
        .catch((err) => console.log('action error', err));

    }
}

const viewWeekStatistic = (chosenDate) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        
        //- call api
        return api.viewWeekStatistic(chosenDate)
        .then((response) => {
            console.log('return from action viewWeekStatistic', response)
            dispatch(basicActions.getWeekList(response));
        })
        .catch((err) => console.log('action error', err));

    }
}

const viewMonthStatistic = (chosenMonth) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        
        //- call api
        return api.viewMonthStatistic(chosenMonth)
        .then((response) => {
            console.log('return from action viewMonthStatistic', response)
            dispatch(basicActions.getMonthList(response));
        })
        .catch((err) => console.log('action error', err));

    }
}

const viewMonthLogtime = (chosenMonth) => {
    return (dispatch) => {
        //- loading
        dispatch(basicActions.getData());
        
        //- call api
        return api.viewMonthLogtime(chosenMonth)
        .then((response) => {
            console.log('return from action viewMonthLogtime', response)
            dispatch(basicActions.getMonthLogTime(response));
        })
        .catch((err) => console.log('action error', err));

    }
}

export {
    login,
    update,
    scan,
    viewWeekStatistic,
    viewMonthStatistic,
    viewMonthLogtime
}