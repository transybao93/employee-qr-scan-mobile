import {
    FETCHING_DATA,
    FETCHING_DATA_SUCCESS,
    LOADING,
    WEEK_LIST,
    MONTH_LIST,
    MONTH_LOGTIME
} from './types';


export const getData = () => {
    return {
        type: FETCHING_DATA
    }
}

export const showLoading = () => {
    return {
        type: LOADING
    }
}

export const getDataSuccess = (data) => {
    console.log('here at basic action', data);
    return {
        type: FETCHING_DATA_SUCCESS,
        data
    }
}

export const getWeekList = (data) => {
    console.log('here at basic action getWeekList', data);
    return {
        type: WEEK_LIST,
        weekList: data
    }
}

export const getMonthList = (data) => {
    console.log('here at basic action getMonthList', data);
    return {
        type: MONTH_LIST,
        monthList: data
    }
}

export const getMonthLogTime = (data) => {
    console.log('here at basic action getMonthLogTime', data);
    return {
        type: MONTH_LOGTIME,
        monthLogtime: data
    }
}