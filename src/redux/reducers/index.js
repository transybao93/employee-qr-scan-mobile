import { combineReducers } from 'redux';
import BaseNavigation from '../../Router';
//- child reducer
import userReducer from './userReducer';

export default combineReducers({
    navigation: (state, action) => BaseNavigation.router.getStateForAction(action, state),
    state: (state = {}) => state,
    userReducer: userReducer
});
