import {
    FETCHING_DATA,
    FETCHING_DATA_SUCCESS,
    FETCHING_DATA_FAIL,
    WEEK_LIST,
    MONTH_LIST,
    MONTH_LOGTIME
} from '../actions/types'

const initialState = {
    data: [],
    dataFetched: false,
    isFetching: false,
    error: false
}

export default function userReducer (state = initialState, action) {
    switch(action.type) {
        case FETCHING_DATA:
        {
            return {
                ...state,
                data: [],
                isFetching: true
            }
        }
        //- main area
        case FETCHING_DATA_SUCCESS:
        {
            return {
                ...state,
                data: action.data,
                isFetching: false
            }
        }
        case WEEK_LIST:
        {
            console.log('here in user reducer with type WEEK_LIST', action.weekList)
            return {
                ...state,
                isFetching: false,
                weekList: action.weekList
            }
        }
        case MONTH_LIST:
        {
            console.log('here in user reducer with type MONTH_LIST', action.monthList)
            return {
                ...state,
                isFetching: false,
                monthList: action.monthList
            }
        }
        case MONTH_LOGTIME:
        {
            console.log('here in user reducer with type MONTH_LOGTIME', action.monthLogtime)
            return {
                ...state,
                isFetching: false,
                monthLogtime: action.monthLogtime
            }
        }
        default:
            return state;
    }
}