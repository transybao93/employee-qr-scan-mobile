import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import logger from 'redux-logger';
import reducers from './redux/reducers';

// const logger = createLogger({
//     // ...options
//     level = 'log',
//     logErrors = true,
//     diff = true
// });

const store = createStore(reducers, {}, applyMiddleware(ReduxThunk, logger));

export default store;
